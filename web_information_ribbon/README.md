Ce module permet de configurer :

- des différents types de ruban d'information en configurant le nom, le message à afficher, la couleur du fond et du texte.

![](./doc_images/Capture%20d’écran%20de%202022-04-28%2011-06-11.png)

- d'afficher un ruban de ce type, en fonction de la configuration choisie :  

![](./doc_images/Capture%20d’écran%20de%202022-04-28%2011-06-37.png)
