{
    "name": "Web Information Ribbon",
    "summary": "Ajoute un ruban en dessous du menu, utile pour prévenir d'une maintenance à venir",
    "author": "Coopaname",
    "category": "Extra Tools",
    "version": "12.0.0.0.0",
    "license": "AGPL-3",
    "depends": ["web"],
    "installable": True,
    "data": [
        "views/information_ribbon.xml",
        "views/res_config_settings.xml",
        "views/webclient_bootstrap.xml",
        "security/ir.model.access.csv",
        "security/ir.rule.xml",
        "templates/information_ribbon.xml",
    ],
}
