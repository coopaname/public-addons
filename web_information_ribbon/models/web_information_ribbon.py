from odoo import api, models, fields


class WebInformationRibbon(models.Model):
    _name = "web.information.ribbon"
    _description = "Information Ribbon"

    name = fields.Char(string="Name")
    message = fields.Char(string="Message")
    background_color = fields.Char(string="Background Color")
    text_color = fields.Char(string="Text Color")
