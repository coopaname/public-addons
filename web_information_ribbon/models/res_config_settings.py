from odoo import fields, models, api


class ResConfigSettings(models.TransientModel):

    _inherit = "res.config.settings"

    web_information_ribbon_id = fields.Many2one(
        "web.information.ribbon", string="Information Ribbon", required=False
    )

    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        res.update(
            web_information_ribbon_id=self.env["ir.config_parameter"]
            .sudo()
            .get_param("web_information_ribbon.web_information_ribbon_id", default=None)
        ),
        return res

    def set_values(self):
        params = self.env["ir.config_parameter"].sudo()
        ribbon = self.web_information_ribbon_id
        ribbon_params = dict(
            display_information_ribbon=len(ribbon) > 0,
            information_ribbon_message=ribbon.message,
            information_ribbon_background_color=ribbon.background_color,
            information_ribbon_text_color=ribbon.text_color,
        )
        for k, v in ribbon_params.items():
            params.set_param(f"web_information_ribbon.{k}", v)

        return super().set_values()
