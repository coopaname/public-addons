from odoo import models


class IrHttp(models.AbstractModel):
    _inherit = "ir.http"

    def get_information_ribbon_values(self):
        display_information_ribbon = (
            self.env["ir.config_parameter"]
            .sudo()
            .get_param(
                "web_information_ribbon.display_information_ribbon", default=False
            )
        )
        try:
            display_information_ribbon = display_information_ribbon.lower()
        except AttributeError:
            pass

        return dict(
            display_information_ribbon=bool(
                display_information_ribbon in ["yes", "true", "y", "1"]
            ),
            information_ribbon_message=(
                self.env["ir.config_parameter"]
                .sudo()
                .get_param(
                    "web_information_ribbon.information_ribbon_message", default=""
                )
            ),
            information_ribbon_background_color=(
                self.env["ir.config_parameter"]
                .sudo()
                .get_param(
                    "web_information_ribbon.information_ribbon_background_color",
                    default="",
                )
            ),
            information_ribbon_text_color=(
                self.env["ir.config_parameter"]
                .sudo()
                .get_param(
                    "web_information_ribbon.information_ribbon_text_color", default=""
                )
            ),
        )

    def webclient_rendering_context(self):
        res = super().webclient_rendering_context()
        res.update(self.get_information_ribbon_values())
        return res
