{
    "name": "Country Origin Product Invoice",
    "summary": "Ajoute un champs Pays d'origine aux lignes de factures",
    "author": "Coopaname",
    "category": "Accounting",
    "version": "12.0.0.0.0",
    "license": "AGPL-3",
    "depends": ["account"],
    "data": [
        "views/account_move_line.xml",
        "views/account_invoice_line.xml",
        "views/product_product.xml",
    ],
    "installable": True,
    "application": False,
}
