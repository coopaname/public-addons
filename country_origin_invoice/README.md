Ce module permet d'ajouter un champs pays d'achat sur les lignes des factures.

La champs apparaît comme ceci :

![](./doc_images/Capture%20d’écran%20de%202022-04-28%2010-47-32.png)

Le produit de vente peut être configuré pour rendre le champs obligatoire :

![](./doc_images/Capture%20d’écran%20de%202022-04-28%2010-48-22.png)

En choisissant "Marchandises", le champs devient alors requis.

![](./doc_images/Capture%20d’écran%20de%202022-04-28%2010-47-47.png)