from odoo import api, models


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    @api.model
    def invoice_line_move_line_get(self):
        self.ensure_one()
        res = super().invoice_line_move_line_get()

        for inv_line in self.invoice_line_ids:
            for i, move_line in enumerate(res):
                if move_line["invl_id"] == inv_line.id:
                    res[i]["country_id"] = inv_line.country_id.id

        return res
