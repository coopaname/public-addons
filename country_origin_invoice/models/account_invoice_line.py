from odoo import fields, models, api


class AccountInvoiceLine(models.Model):
    _inherit = "account.invoice.line"

    country_id = fields.Many2one("res.country", "Country")

    is_country_required = fields.Boolean(
        string="Origin country is required", compute="_compute_is_country_required"
    )

    @api.onchange("product_id")
    def _compute_is_country_required(self):
        for aml in self:
            aml.is_country_required = aml.product_id.is_country_required
