from odoo import fields, models, api


class ProductProduct(models.Model):
    _inherit = "product.product"

    is_country_required = fields.Boolean(
        string="Origin country is required", default=False
    )

    @api.model
    def _convert_prepared_anglosaxon_line(self, line, partner):
        res = super()._convert_prepared_anglosaxon_line(line, partner)
        res["country_id"] = line.get("country_id", False)
        return res
