from odoo import fields, models


class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    country_id = fields.Many2one("res.country", "Country")
