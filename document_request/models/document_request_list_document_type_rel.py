from odoo import models, fields, api


class DocumentRequestListDocumentTypeRel(models.AbstractModel):
    _name = "document.request.list.document.request.type.rel"
    _description = "Relation between a Document List Request and a Document Type to which is attached a Document"

    document_request_list_id = fields.Many2one(
        comodel_name="document.request.list", string="Document Request List"
    )
    document_request_type_id = fields.Many2one(
        comodel_name="document.request.type", string="Document Request Type"
    )
    name = fields.Char(string="Name", compute="_compute_name")
    accepted_document_list = fields.Char(
        "List of accepted documents",
        help="Description of the different documents accepted. For instance, for identity purpose, you might accept identity card or driver license.",
        compute="_compute_accepted_document_list",
    )
    necessity_condition = fields.Char(
        "Necessity condition",
        help="If this conditions are fulfilled, this document type is necessary",
        compute="_compute_necessity_condition",
    )

    @api.depends("document_request_type_id")
    def _compute_name(self):
        for rec in self:
            rec.name = rec.document_request_type_id.name

    @api.depends("document_request_type_id")
    def _compute_accepted_document_list(self):
        for rec in self:
            rec.accepted_document_list = (
                rec.document_request_type_id.accepted_document_list
            )

    @api.depends("document_request_type_id")
    def _compute_necessity_condition(self):
        for rec in self:
            rec.necessity_condition = rec.document_request_type_id.necessity_condition
