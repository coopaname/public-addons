import logging
from odoo import models, fields, api


class DocumentListRequest(models.Model):
    _name = "document.request.list"
    _inherit = ["mail.thread"]
    _description = "Document List Request"

    name = fields.Char(string="Name", required=True)
    display_name = fields.Char(compute="_compute_display_name")
    create_date = fields.Datetime("Asked on", index=True, readonly=True)
    create_uid = fields.Many2one(
        "res.users", string="Created by", index=True, readonly=True
    )
    document_types_ids = fields.Many2many(
        comodel_name="document.request.type",
        relation="document_request_document",
        string="Document types requested",
        help="Includes all document types needed for this document request.",
    )
    document_ids = fields.One2many(
        comodel_name="document.request.document",
        inverse_name="document_request_list_id",
        string="Documents",
    )
    partner_id = fields.Many2one(
        comodel_name="res.partner",
        string="Requested Partner",
        help="Partner to request the document list to",
    )
    is_complete = fields.Boolean(
        string="Is Request Complete", compute="_compute_is_complete"
    )
    need_validation = fields.Boolean(string="Need Validation")
    is_valid = fields.Boolean(string="Is Valid", compute="_compute_validity")
    is_invalid = fields.Boolean(string="Is Not Valid", compute="_compute_validity")
    state = fields.Selection(
        [
            ("sent", "Envoyé"),
            ("wait_validation", "To validate"),
            ("valid", "Validated"),
            ("invalid", "Incorrect"),
        ],
        string="Status",
        compute="_compute_state",
        track_visibility="onchange",
        store=True,
    )

    @api.depends("name", "partner_id")
    def _compute_display_name(self):
        for drl in self:
            drl.display_name = f"{drl.partner_id.name} ({drl.name})"

    @api.depends("document_ids")
    def _compute_is_complete(self):
        for drl in self:
            drl.is_complete = all(
                map(lambda doc: len(doc.mapped("attachment_ids")) > 0, drl.document_ids)
            )

    @api.depends("document_ids.is_valid", "document_ids.is_invalid")
    def _compute_validity(self):
        for drl in self:
            drl.is_valid = all(drl.document_ids.mapped("is_valid")) and not any(
                drl.document_ids.mapped("is_invalid")
            )
            drl.is_invalid = any(drl.document_ids.mapped("is_invalid"))

    @api.depends("is_valid", "is_invalid", "need_validation")
    def _compute_state(self):
        for drl in self:
            if drl.is_valid:
                drl.state = "valid"
            elif drl.need_validation:
                drl.state = "wait_validation"
            elif drl.is_invalid:
                drl.state = "invalid"
            else:
                drl.state = "sent"

    @api.multi
    def download(self):
        self.ensure_one()
        return {
            "type": "ir.actions.act_url",
            "url": f"/document_request/{self.id}",
            "target": "self",
        }


class ResPartner(models.Model):

    _inherit = "res.partner"

    document_request_ids = fields.One2many(comodel_name="document.request.list", inverse_name="partner_id")
