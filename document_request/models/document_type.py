from odoo import models, fields


class DocumentType(models.Model):
    _name = "document.request.type"
    _description = "Document Type for Document List Requests"

    name = fields.Char(string="Name", required=True)
    accepted_document_list = fields.Char(
        "List of accepted documents",
        help="Description of the different documents accepted. For instance, for identity purpose, you might accept identity card or driver license.",
    )
    necessity_condition = fields.Char(
        "Necessity condition",
        help="If this conditions are fulfilled, this document type is necessary",
    )
    document_request_list_ids = fields.Many2many(
        comodel_name="document.request.list",
        relation="document_request_document",
        string="Document request lists",
        help="Document request lists that requested this type of document.",
    )
