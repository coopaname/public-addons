import logging

from odoo import models, fields, api


class Document(models.Model):
    _name = "document.request.document"
    _description = "Document of a Document List Request"
    _inherit = "document.request.list.document.request.type.rel"

    is_valid = fields.Boolean(string="Is Valid")
    is_invalid = fields.Boolean(string="Is Not Valid")
    attachment_ids = fields.One2many(
        comodel_name="ir.attachment",
        string="Linked Attachments",
        inverse_name="res_id",
        domain=[("res_model", "=", _name)],
        context=dict(default_res_model="document.request.document"),
    )
    image_attachment_ids = fields.One2many(
        comodel_name="ir.attachment",
        string="Linked Image Attachments",
        compute="_compute_attachment_ids",
    )
    pdf_attachment_ids = fields.One2many(
        comodel_name="ir.attachment",
        string="Linked PDF Attachments",
        compute="_compute_attachment_ids",
    )
    other_attachment_ids = fields.One2many(
        comodel_name="ir.attachment",
        string="Linked Other Attachments",
        compute="_compute_attachment_ids",
    )

    def _filter_attachment(self, attachments, mimetypes, exclude=False):
        if exclude:
            return filter(lambda a: a.mimetype not in mimetypes, attachments)
        else:
            return filter(lambda a: a.mimetype in mimetypes, attachments)

    def _prepare_filtered_attachments(self, filtered_attachments):
        return list(map(lambda a: a.id, filtered_attachments))

    @api.depends("attachment_ids")
    def _compute_attachment_ids(self):
        for d in self:
            image_mime_types = ["image/png", "image/jpeg"]
            d.image_attachment_ids = self._prepare_filtered_attachments(
                self._filter_attachment(d.attachment_ids, image_mime_types)
            )
            pdf_mime_types = ["application/pdf"]
            d.pdf_attachment_ids = self._prepare_filtered_attachments(
                self._filter_attachment(d.attachment_ids, pdf_mime_types)
            )
            d.other_attachment_ids = self._prepare_filtered_attachments(
                self._filter_attachment(
                    d.attachment_ids, image_mime_types + pdf_mime_types, exclude=True
                )
            )

    @api.multi
    def write(self, vals):
        # we need this as well as the onchange to force reset to False even if the fields are in read-only mode.
        if "attachment_ids" in vals and vals["attachment_ids"]:
            vals["is_valid"] = False
            vals["is_invalid"] = False
        return super().write(vals)

    @api.onchange("is_valid")
    def is_valid_change(self):
        if len(self.attachment_ids) == 0:
            self.is_valid = False
        if self.is_valid:
            self.is_invalid = False

    @api.onchange("is_invalid")
    def is_invalid_change(self):
        if self.is_invalid:
            self.is_valid = False

    @api.onchange("attachment_ids")
    def attachment_ids_change(self):
        # we need this as well as the write to reset it to False as soon as possible so user can see it.
        self.is_valid = False
        self.is_invalid = False

    def _get_document_request_list_view(self):
        return {
            "type": "ir.actions.act_window",
            "view_mode": "form",
            "view_type": "form",
            "res_model": "document.request.list",
            "res_id": self.document_request_list_id.id,
            "target": "main",
        }

    @api.multi
    def validate_document(self):
        self.ensure_one()
        self.is_valid = True
        self.is_invalid = False
        self.document_request_list_id.need_validation = False
        return self._get_document_request_list_view()

    @api.multi
    def invalidate_document(self):
        self.ensure_one()
        self.is_valid = False
        self.is_invalid = True
        self.document_request_list_id.need_validation = False
        return self._get_document_request_list_view()

    @api.multi
    def close(self):
        self.ensure_one()
        return self._get_document_request_list_view()
