from odoo import models, fields


class DocumentListRequestTemplate(models.Model):
    _name = "document.request.list.template"
    _description = "Template for Document List Requests"

    name = fields.Char(string="Name", required=True)
    document_types_ids = fields.Many2many(
        comodel_name="document.request.type",
        string="Document types requested",
        help="Includes all document types needed for this document request.",
    )
