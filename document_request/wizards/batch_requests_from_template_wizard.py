from odoo import api, fields, models


class BatchRequestsFromTemplateWizard(models.TransientModel):
    _name = "batch.requests.from.template.wizard"
    _description = "Wizard to handle document requests"

    template_id = fields.Many2one("document.request.list.template")
    partner_ids = fields.Many2many("res.partner")

    @api.model
    def default_get(self, field_names):
        defaults = super().default_get(field_names)
        defaults["template_id"] = self.env.context.get("active_id", None)
        return defaults

    @api.multi
    def action_generate(self):
        self.ensure_one()

        res_ids = []
        for partner in self.partner_ids:
            drl = self.env["document.request.list"].create(
                dict(
                    name=self.template_id.name,
                    document_types_ids=[
                        (4, document_type_id, False)
                        for document_type_id in self.template_id.document_types_ids.ids
                    ],
                    partner_id=partner.id,
                )
            )
            drl.message_subscribe([partner.id])
            res_ids.append(drl.id)

        return {
            "type": "ir.actions.act_window",
            "name": ", ".join(map(str, res_ids)),
            "view_mode": "tree,form",
            "view_id": False,
            "view_type": "form",
            "res_model": "document.request.list",
            "target": "current",
            "domain": [("id", "in", res_ids)],
        }
