import base64
from io import BytesIO
from zipfile import ZipFile

from odoo.http import request, Controller, route


class EventAttendeesheetExportController(Controller):
    @route(
        ['/document_request/<model("document.request.list"):document_request_list>'],
        type="http",
        auth="user",
    )
    def download_documents(self, document_request_list=None, **args):
        with BytesIO() as zipfile:
            with ZipFile(zipfile, "w") as zip:
                for doc in document_request_list.document_ids:
                    for i, attached in enumerate(doc.attachment_ids):
                        num_suffix = "" if len(doc.attachment_ids) <= 1 else f" {i+1}"
                        extension = attached.datas_fname.split(".")[-1]
                        zip.writestr(
                            f"{document_request_list.partner_id.lastname} "
                            f"{document_request_list.partner_id.firstname} "
                            f"{doc.document_request_type_id.name}{num_suffix}.{extension}",
                            base64.b64decode(attached.datas),
                        )

            return request.make_response(
                zipfile.getvalue(),
                headers=[
                    ("Content-Type", "application/zip"),
                    ("Content-Disposition", 'attachment; filename="Documents.zip"'),
                ],
            )
