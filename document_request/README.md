Ce module permet de configurer :

- des types de documents, d'indiquer quels documents sont acceptés. Par exemple pour un document type "Pièce d'identité", nous acceptons CNI, Passeport...

![](./doc_images/Capture%20d’écran%20de%202022-04-28%2010-54-21.png)

- des modèles de demande de document. Par exemple, pour une intégration Coopaname, nous demandons plusieurs types de documents (Pièce d'identité, Attestation Pôle Emploi, etc.)  

![](./doc_images/Capture%20d’écran%20de%202022-04-28%2010-55-47.png)

- d'effectuer une demande de type "Intégration Coopaname" à plusieurs contacts.

![](./doc_images/Capture%20d’écran%20de%202022-04-28%2011-00-30.png)

- de suivre les demandes en cours

![](./doc_images/Capture%20d’écran%20de%202022-04-28%2011-03-35.png)

- de voir rapidement le document attaché par la personne, de le valider/invalider

![](./doc_images/Capture%20d’écran%20de%202022-04-28%2011-03-40.png)
